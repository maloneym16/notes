PYTHON INTERPRETER
    Enter | python
    Exit | exit ()
    Run a file | python file_name.py

GENERAL
    Comment | #  OR command+/
    Check if two variables are the same | ==
    Check the type of a variable | type(variable)
    Display the value of a variable | print(variable)
    Choose a random int within a range | from randomvariable import randint(minint, maxint)
    Loop an action a specific number of times | for variable in range(int)
    Inturrupt infinitely looping code | command+c
    Return the memory location of the provided argument | id()
    print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False)

STRINGS
Count characters in a str: len(str)
Uppercase a string: upper(str)
Concatenate two (or more) strings: +   
Change a str to an int: int(str)
Lowercase all characters: strNew = strOld.lower()
Replace a substring with another in a string: new_string = string.replace(oldsub, newsub)
Display a message: print("str")
Prompt for input and return the input entered: input("str for prompt")
Create a series of int from a range: range(startint, stopint, step)

INTEGERS
Add two (or more) integers: +
Other int operators: -,  *,  %

LISTS
Add an element to the end of a list: .append()
Add an element to a specific index position in a list: .insert()
Adds all the elements of an iterable (list, tuple, string etc.) to the end of the list: .extend()
Remove an element from a list at the specified index position: .pop()
Remove the first element from a list that matches parameter's value: .remove()
Clear out a list: .clear()

DICTIONARIES / DICT - dictname[key][subdictkey][etc]
Return a list of the keys: dictname.keys()
Return a list of the values: dictname.values()
Return a list of key-value pairs inside tuples: dictname.items()
Remove an element at the specified index position: dictname[key].pop()
Returns and remove the most reciently added key-value pair: dictname[key].popitem()
Create a new dictionary object with the supplied key-value pairs: {key: value}
Update an item in a dictionary: dictname[key] = value
Update multiple key-value pairs in a dictionary: dictname.update(newpairs)
Remove an item from a dictionary: del dictname[key]
Get an item from a dictionary 
	dictname[key]  ~  causes an error if key doesnt exist
	dictname.get(key, "message")  ~  returns message if key doesnt exist, None if blank
