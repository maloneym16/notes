from django.shortcuts import render, get_object_or_404
from examples.models import Example

# VIEW SINGLE
def show_model(request, id):
    example = get_object_or_404(Example, id=id)   # Get the data
    context = {   # Put the data in a context
        "example_object": example,
    }
    return render(request, "example/detail.html", context)   # Render HTML with context data


# VIEW ALL
def list_models(request):
    example = Example.objects.all()
    context = {
        "list_models": example,
    }
    return render(request, "example/list.html", context)
    

# EDIT EXISTING
def edit_example(request, id)
    # find the id to edit
        # show it
        # grab the form


# DELETE EXISTING
def delete_example(request, id)


# CREATE NEW
def create_example(request)
    # if this request is a POST
        # grab the form
            # form = RecipeForm(request.POST)
    # is the form valid
        .save()  # if so, save it
            # and take me to list page
