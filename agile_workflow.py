GENERAL TERMS
	MVP - Minimal Viable Product
	User Experience (UX) 
	User Interface (UI)

ROLES
	Client
	Product manager/owner
	Scrum: Project manager, principal engineer, CTO
	Development Team of engineers

STATEMENTS
	AAU - "as a user"
	AASE - "as a software engineer" aka Stories
